/**
 *
 * * generate TMDb api key
 * * create .env file in the root of your project directory
 * * create a variable in the .env file named REACT_APP_API_KEY and assign your TMDb key to it
 *
 */
export const apiRoot = "http://api.themoviedb.org/3";
export const apiPopular = "movie/popular";
export const apiParams = {
  api_key: process.env.REACT_APP_API_KEY,
  language: "en-US"
};
