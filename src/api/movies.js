import axios from "axios";
import { apiRoot } from "./config";

export default axios.create({
  baseURL: `${apiRoot}`
});
