import styled from "styled-components";

export const MovieSliderOutput = styled.output`
  display: block;
  margin-bottom: 0.3em;
`;

export const MovieSlider = styled.input`
  -webkit-appearance: none;
  height: 25px;
  background: #000;
  outline: none;
  margin: 0;
  ::-webkit-slider-thumb {
    -webkit-appearance: none; /* Override default look */
    appearance: none;
    cursor: pointer;
    width: 25px;
    height: 25px;
    background-color: #000;
    background-image: repeating-linear-gradient(
      45deg,
      transparent,
      transparent 2px,
      rgba(255, 255, 255, 1) 2px,
      rgba(255, 255, 255, 1) 4px
    );
  }

  ::-moz-range-thumb {
    cursor: pointer;
    width: 25px;
    height: 25px;
    background-color: #000;
    background-image: repeating-linear-gradient(
      45deg,
      transparent,
      transparent 2px,
      rgba(255, 255, 255, 1) 2px,
      rgba(255, 255, 255, 1) 4px
    );
  }
`;
