import React, { useState, memo } from "react";
import { connect } from "react-redux";
import { getMovieRating } from "../../store/movies";
import { MovieSlider, MovieSliderOutput } from "./MovieRatingFilterStyles";

const slider = React.createRef();

const SliderOutput = memo(({ output }) => {
  return (
    <MovieSliderOutput htmlFor="rating-slide" id="volume">
      rating : {output} or higher
    </MovieSliderOutput>
  );
});

const MovieRatingFilter = ({ movieRating, onUpdate }) => {
  const [slideVal, setSlideVal] = useState(movieRating);
  return (
    <React.Fragment>
      <MovieSlider
        ref={slider}
        type="range"
        min="0"
        max="10"
        value={slideVal}
        id="rating-slide"
        step=".5"
        onChange={e => {
          if (e.target.value !== movieRating) {
            setSlideVal(e.target.value);
            onUpdate(e.target.value);
          }
        }}
      />
      <SliderOutput output={slideVal} />
    </React.Fragment>
  );
};

const mapStateToProps = state => {
  return {
    movieRating: getMovieRating(state)
  };
};

export default connect(mapStateToProps)(
  memo(
    MovieRatingFilter,
    (prevProps, nextProps) => nextProps.movieRating === slider.current.value
  )
);
