import styled from "styled-components";

export const ResponsiveImage = styled.div`
  width: 100%;
  padding-top: 150%;
  position: relative;
  background: black;
`;

export const Picture = styled.div`
  position: absolute;
  top: 0px;
  right: 0px;
  bottom: 0px;
  left: 0px;
  overflow: hidden;
`;
export const Image = styled.img`
  width: 100%;
`;
