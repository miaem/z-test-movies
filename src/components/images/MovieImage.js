import React, { memo } from "react";
import { connect } from "react-redux";
import { getImageBreakpoints } from "../../store/images";
import { ResponsiveImage, Image, Picture } from "./MovieImageStyles";

const MovieImage = ({ image, title, breakpoints }) => {
  const picture = () => {
    return (
      <ResponsiveImage>
        <Picture>
          <Image
            srcSet={image.set}
            sizes={breakpoints}
            src={image.src}
            alt={title}
          />
        </Picture>
      </ResponsiveImage>
    );
  };

  return picture();
};

const mapStateToProps = state => {
  return {
    breakpoints: getImageBreakpoints(state)
  };
};

export default connect(mapStateToProps)(memo(MovieImage));
