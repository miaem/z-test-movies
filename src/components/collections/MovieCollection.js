import _ from "lodash";
import React, { memo } from "react";
import { connect } from "react-redux";
import { getMoviesByRating } from "../../store/movies";
import { MovieCard } from "../card";
import { MovieList, MovieListItem } from "./MovieStyles";

const MovieCards = ({ movies }) => {
  return movies.map(item => {
    return (
      <MovieListItem key={item.id}>
        <MovieCard
          id={item.id}
          genres={item.genre_ids}
          title={item.title}
          image={item.srcset}
        />
      </MovieListItem>
    );
  });
};

const MovieCollection = props => {
  return (
    props.movies.length > 0 && (
      <MovieList>
        <MovieCards {...props} />
      </MovieList>
    )
  );
};

const mapStateToProps = () => {
  const getMoviesInstance = getMoviesByRating();
  return state => {
    return { movies: getMoviesInstance(state) };
  };
};

export default connect(mapStateToProps)(
  memo(MovieCollection, (prevProps, nextProps) => {
    return _.isEqual(prevProps, nextProps);
  })
);
