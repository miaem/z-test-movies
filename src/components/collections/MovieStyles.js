import styled from "styled-components";

export const Row = styled.section`
  display: flex;
  flex-wrap: wrap;
  padding: 0 1em;
  h2 {
    margin: 0;
  }
`;

export const MovieList = styled.ul`
  display: flex;
  flex-wrap: wrap;
  padding: 1em;
  list-style-type: none;
  padding: 0 1em;
  width: 100%;
  ul {
    display: inline;
    font-size: 0.7em;
    list-style-type: none;
    padding: 0;
    li {
      display: inline-block;
      &:first-child::before {
        content: " ";
        white-space: pre;
      }
      &:not(:last-child)::after {
        content: ", ";
        white-space: pre;
      }
    }
  }
  h3 {
    margin-top: 0;
  }
`;

export const MovieListItem = styled.li`
  padding: 0.3em;
  flex: 0 0 100%;
  max-width: 100%;
  @media (min-width: 576px) {
    flex: 0 0 25%;
    max-width: 25%;
  }
  @media (min-width: 768px) {
    flex: 0 0 16.666667%;
    max-width: 16.666667%;
  }
`;
