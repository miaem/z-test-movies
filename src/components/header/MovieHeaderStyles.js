import styled from "styled-components";

export const Header = styled.header`
  font-family: "Plaster", sans-serif;
  background-color: #000;
  color: #fff;
  padding: 1em;
  h1 {
    padding: 0 0.2em;
    margin: 0;
  }
`;
