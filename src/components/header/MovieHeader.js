import React, { memo } from "react";
import { Header } from "./MovieHeaderStyles";

const MovieHeader = ({ title }) => {
  return (
    <Header>
      <h1>{title}</h1>
    </Header>
  );
};

export default memo(MovieHeader);
