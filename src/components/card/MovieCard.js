import React, { memo } from "react";
import { MovieImage } from "../images";
import { Card, CardContent, Genres, GenreLabel } from "./MovieCardStyles";

const CardTitle = memo(({ title }) => {
  return <h3>{title}</h3>;
});

const GenreList = memo(({ genres, label }) => {
  return (
    <Genres>
      <GenreLabel>{label}</GenreLabel>
      {Object.keys(genres).length > 0 && (
        <ul>
          {Object.keys(genres).map(genre => {
            return <li key={genres[genre].id}>{genres[genre].title}</li>;
          })}
        </ul>
      )}
    </Genres>
  );
});

const MovieCard = props => {
  const card = () => {
    return (
      <Card>
        <MovieImage {...props} />
        <CardContent>
          <CardTitle {...props} />
          <GenreList {...props} label="Genres" />
        </CardContent>
      </Card>
    );
  };

  return card();
};

export default memo(MovieCard);
