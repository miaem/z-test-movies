import styled from "styled-components";

export const Card = styled.div`
  word-break: break-word;
  display: flex;
  flex-direction: column;
  border: solid 1px #000;
  height: 100%;
  overflow: hidden;
  box-shadow: 1px 1px rgba(1, 1, 1, 0.25);
  transition: all 0.2s ease-in-out;
  :hover {
    box-shadow: 1px 1px rgba(1, 1, 1, 1);
    img {
      transition: all 0.2s ease-in-out;
      transform: scale(1.05);
    }
  }
`;

export const CardContent = styled.div`
  display: flex;
  height: 100%;
  flex-direction: column;
  align-items: end;
  justify-content: space-between;
  padding: 1em;
`;

export const Genres = styled.div`
  display: block;
  width: 100%;
`;

export const GenreLabel = styled.span`
  text-align: center;
  font-weight: 700;
  font-size: 0.8em;
  &::after {
    content: " - ";
    white-space: pre;
  }
`;
