import styled from "styled-components";

export const Row = styled.section`
  display: flex;
  flex-wrap: wrap;
  padding: 0 1em;
  h2 {
    margin: 0;
  }
`;

export const ColfullWidth = styled.div`
  flex: 0 0 100%;
  max-width: 100%;
  padding: 0.3em;
`;

export const ColfullWidthDivider = styled(ColfullWidth)`
  &::after {
    content: "";
    display: block;
    width: 100%;
    background: #000;
    height: 1px;
  }
`;

export const Label = styled.label`
  display: block;
  margin-bottom: 0.3em;
`;
