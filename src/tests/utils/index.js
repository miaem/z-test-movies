import configureStore from "redux-mock-store";
import thunkMiddleware from "redux-thunk";

export const findAtrr = (component, attr) => {
  const wrapper = component.find(`[data-test='${attr}']`);
  return wrapper;
};

export const mockStore = initialState => {
  const middlewares = [thunkMiddleware];
  const mockStore = configureStore(middlewares);
  return mockStore(initialState);
};
