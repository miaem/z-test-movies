import mockAxios from "axios";
import { mockStore } from "./../../../utils";
import { FETCH_IMAGES, GET_IMAGES } from "./../../../../store/images/types";
import { fetchImages, getImages } from "../../../../store/images/actions";
import { apiParams } from "../../../../api/config";

const initialState = {
  images: {
    isLoaded: false,
    breakpoints: "..",
    bySize: {},
    allSizes: []
  }
};

let store;

beforeAll(() => {
  store = mockStore(initialState);
});

afterEach(() => {
  store.clearActions();
});

test("fetch and dispatch image action", async () => {
  mockAxios.get.mockResolvedValue(() => {
    Promise.resolve({ loaded: true });
  });

  const expectedActions = [
    {
      type: FETCH_IMAGES,
      loaded: true
    }
  ];

  await store.dispatch(fetchImages());

  expect(mockAxios.get).toHaveBeenCalledTimes(1);
  expect(mockAxios.get).toHaveBeenCalledWith("/configuration", {
    params: { ...apiParams }
  });
  expect(store.getActions()).toEqual(expectedActions);
});

test("set, get and dispatch image action", () => {
  let response = {
    images: {
      secure_base_url: "https://image.tmdb.org/t/p/"
    }
  };

  const expectedActions = [
    {
      type: GET_IMAGES,
      payload: {
        secure_base_url: "https://image.tmdb.org/t/p/"
      },
      raw: { secure_base_url: "https://image.tmdb.org/t/p/" }
    }
  ];

  store.dispatch(getImages(response));
  expect(store.getActions()).toEqual(expectedActions);
});
