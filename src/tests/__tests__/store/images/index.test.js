import { imagesReducer, getImageBreakpoints } from "./../../../../store/images";
import { FETCH_IMAGES, GET_IMAGES } from "./../../../../store/images/types";

const INITIAL_STATE = {
  isLoaded: false,
  breakpoints: "(min-width: 576px) 25vw, (min-width: 768px) 16.666667vw, 100vw",
  bySize: {},
  allSizes: []
};

test("render initial state", () => {
  expect(imagesReducer(undefined, {})).toEqual(INITIAL_STATE);
});

test("handle FETCH_IMAGES", () => {
  expect(
    imagesReducer(INITIAL_STATE, { type: FETCH_IMAGES, loaded: true })
  ).toEqual({
    ...INITIAL_STATE,
    isLoaded: true
  });
});

test("handle GET_IMAGES", () => {
  const images = {
    secure_base_url: "https://image.tmdb.org/t/p/",
    poster_sizes: ["w92", "original"]
  };
  expect(
    imagesReducer(INITIAL_STATE, {
      type: GET_IMAGES,
      payload: images,
      raw: images
    })
  ).toEqual({
    ...INITIAL_STATE,
    bySize: {
      w92: {
        url: "https://image.tmdb.org/t/p/w92",
        size: "92"
      },
      original: {
        url: "https://image.tmdb.org/t/p/original",
        size: "original"
      }
    },
    allSizes: ["w92", "original"]
  });
});

test("selector getImageBreakpoints", () => {
  expect(
    getImageBreakpoints({
      images: { ...INITIAL_STATE }
    })
  ).toEqual(INITIAL_STATE.breakpoints);
});
