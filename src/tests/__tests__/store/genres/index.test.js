import { genresReducer } from "./../../../../store/genres";
import {
  FETCH_GENRES,
  GET_GENRES,
  ADD_MOVIE
} from "./../../../../store/genres/types";

const INITIAL_STATE = {
  isLoaded: false,
  byId: {},
  allIds: []
};

test("render initial state", () => {
  expect(genresReducer(undefined, {})).toEqual(INITIAL_STATE);
});

test("handle FETCH_GENRES", () => {
  expect(
    genresReducer(INITIAL_STATE, { type: FETCH_GENRES, loaded: true })
  ).toEqual({
    ...INITIAL_STATE,
    isLoaded: true
  });
});

test("handle GET_GENRES", () => {
  expect(
    genresReducer(INITIAL_STATE, {
      type: GET_GENRES,
      payload: [{ id: 28, name: "Action" }],
      raw: [28]
    })
  ).toEqual({
    ...INITIAL_STATE,
    byId: {
      28: {
        id: 28,
        name: "Action",
        movies: []
      }
    },
    allIds: [28]
  });
});

test("handle ADD_MOVIE", () => {
  const initialMovies = {
    isLoaded: true,
    byId: {
      "28": {
        id: 28,
        name: "Action",
        movies: []
      },
      allIds: [28]
    }
  };
  expect(
    genresReducer(initialMovies, {
      type: ADD_MOVIE,
      genreId: 28,
      movie: { id: "012345", title: "Another action movie" }
    })
  ).toEqual({
    ...initialMovies,
    byId: {
      ...initialMovies.byId,
      [28]: {
        ...initialMovies.byId[28],
        movies: [
          ...initialMovies.byId[28].movies,
          { id: "012345", title: "Another action movie" }
        ]
      }
    }
  });
});
