import mockAxios from "axios";
import { mockStore } from "./../../../utils";
import {
  FETCH_GENRES,
  GET_GENRES,
  ADD_MOVIE
} from "./../../../../store/genres/types";
import {
  fetchGenres,
  getGenres,
  addMovieToGenre
} from "../../../../store/genres/actions";
import { apiParams } from "../../../../api/config";

const initialState = {
  genres: {
    isLoaded: false,
    byId: {},
    allIds: []
  }
};

let store;

beforeEach(() => {
  store = mockStore(initialState);
});

afterEach(() => {
  store.clearActions();
});

test("fetch and dispatch image action", async () => {
  mockAxios.get.mockResolvedValue(() => {
    Promise.resolve({ loaded: true });
  });

  const expectedActions = [
    {
      type: FETCH_GENRES,
      loaded: true
    }
  ];

  await store.dispatch(fetchGenres());

  expect(mockAxios.get).toHaveBeenCalledTimes(1);
  expect(mockAxios.get).toHaveBeenCalledWith("/genre/movie/list", {
    params: { ...apiParams }
  });
  expect(store.getActions()).toEqual(expectedActions);
});

test("set, get and dispatch genre action", () => {
  let response = {
    genres: [{ id: 28, name: "Action" }]
  };

  const expectedActions = [
    {
      type: GET_GENRES,
      payload: [
        {
          id: 28,
          name: "Action"
        }
      ],
      raw: [28]
    }
  ];

  store.dispatch(getGenres(response));
  expect(store.getActions()).toEqual(expectedActions);
});

test("add movie genre action", () => {
  const expectedActions = [
    {
      type: ADD_MOVIE,
      genreId: "28",
      movie: "Test"
    }
  ];

  store.dispatch(addMovieToGenre("28", "Test"));
  expect(store.getActions()).toEqual(expectedActions);
});
