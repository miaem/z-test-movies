import mockAxios from "axios";
import { mockStore } from "./../../../utils";
import {
  FETCH_MOVIES,
  GET_MOVIES,
  MOVIE_RATING
} from "./../../../../store/movies/types";
import { ADD_MOVIE } from "./../../../../store/genres/types";
import {
  fetchMovies,
  getMovies,
  updateMovieRating
} from "../../../../store/movies/actions";
import { apiParams, apiPopular } from "../../../../api/config";

const initialState = {
  movies: {
    isLoaded: false,
    voteRating: 3,
    byId: {},
    allIds: []
  },
  genres: { byId: { 80: { id: 80, name: "Crime", movies: [] } } },
  images: {
    bySize: {
      w92: {
        url: "https://image.tmdb.org/t/p/w92",
        size: "92"
      },
      w154: {
        url: "https://image.tmdb.org/t/p/w154",
        size: "154"
      },
      w185: {
        url: "https://image.tmdb.org/t/p/w185",
        size: "185"
      },
      w342: {
        url: "https://image.tmdb.org/t/p/w342",
        size: "342"
      },
      w500: {
        url: "https://image.tmdb.org/t/p/w500",
        size: "500"
      },
      w780: {
        url: "https://image.tmdb.org/t/p/w780",
        size: "780"
      },
      original: {
        url: "https://image.tmdb.org/t/p/original",
        size: "original"
      }
    },
    allSizes: ["w92", "w154", "w185", "w342", "w500", "w780", "original"]
  }
};

let store;

beforeEach(() => {
  store = mockStore(initialState);
});

afterEach(() => {
  store.clearActions();
});

test("fetch and dispatch movie action", async () => {
  mockAxios.get.mockResolvedValue(() => {
    Promise.resolve({ loaded: true });
  });

  const expectedActions = [
    {
      type: FETCH_MOVIES,
      loaded: true
    }
  ];

  await store.dispatch(fetchMovies());

  expect(mockAxios.get).toHaveBeenCalledTimes(1);
  expect(mockAxios.get).toHaveBeenCalledWith(`${apiPopular}`, {
    params: { ...apiParams, page: 1 }
  });
  expect(store.getActions()).toEqual(expectedActions);
});

test("set, get and dispatch movie action", async () => {
  let response = {
    results: [
      {
        popularity: 534.771,
        vote_count: 4809,
        poster_path: "/udDclJoHjfjb8Ekgsd4FDteOkCU.jpg",
        id: 475557,
        genre_ids: [80],
        title: "Joker",
        vote_average: 8.5
      }
    ]
  };
  const expectedActions = [
    {
      type: ADD_MOVIE,
      genreId: 80,
      movie: {
        id: 475557,
        title: "Joker"
      }
    },
    {
      type: GET_MOVIES,
      payload: {
        "475557": {
          popularity: 534.771,
          vote_count: 4809,
          poster_path: "/udDclJoHjfjb8Ekgsd4FDteOkCU.jpg",
          id: 475557,
          genre_ids: {
            "80": {
              id: 80,
              title: "Crime"
            }
          },
          title: "Joker",
          vote_average: 8.5,
          srcset: {
            set:
              "https://image.tmdb.org/t/p/w92/udDclJoHjfjb8Ekgsd4FDteOkCU.jpg 92w, https://image.tmdb.org/t/p/w154/udDclJoHjfjb8Ekgsd4FDteOkCU.jpg 154w, https://image.tmdb.org/t/p/w185/udDclJoHjfjb8Ekgsd4FDteOkCU.jpg 185w, https://image.tmdb.org/t/p/w342/udDclJoHjfjb8Ekgsd4FDteOkCU.jpg 342w, https://image.tmdb.org/t/p/w500/udDclJoHjfjb8Ekgsd4FDteOkCU.jpg 500w, https://image.tmdb.org/t/p/w780/udDclJoHjfjb8Ekgsd4FDteOkCU.jpg 780w",
            src:
              "https://image.tmdb.org/t/p/original/udDclJoHjfjb8Ekgsd4FDteOkCU.jpg"
          }
        }
      },
      raw: [475557]
    }
  ];

  await store.dispatch(getMovies(response));
  expect(store.getActions()).toEqual(expectedActions);
});

test("update movie action", () => {
  const expectedActions = [
    {
      type: MOVIE_RATING,
      payload: 5
    }
  ];

  store.dispatch(updateMovieRating(5));
  expect(store.getActions()).toEqual(expectedActions);
});
