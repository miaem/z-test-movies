import { moviesReducer } from "./../../../../store/movies";
import {
  FETCH_MOVIES,
  GET_MOVIES,
  MOVIE_RATING
} from "./../../../../store/movies/types";

const INITIAL_STATE = {
  isLoaded: false,
  voteRating: 3,
  byId: {},
  allIds: []
};

test("render initial state", () => {
  expect(moviesReducer(undefined, {})).toEqual(INITIAL_STATE);
});

test("handle FETCH_MOVIES", () => {
  expect(
    moviesReducer(INITIAL_STATE, { type: FETCH_MOVIES, loaded: true })
  ).toEqual({
    ...INITIAL_STATE,
    isLoaded: true
  });
});

test("handle GET_MOVIES", () => {
  expect(
    moviesReducer(INITIAL_STATE, {
      type: GET_MOVIES,
      payload: {},
      raw: []
    })
  ).toEqual({
    ...INITIAL_STATE,
    byId: {},
    allIds: []
  });
});

test("handle MOVIE_RATING", () => {
  expect(
    moviesReducer(INITIAL_STATE, {
      type: MOVIE_RATING,
      payload: 5
    })
  ).toEqual({
    ...INITIAL_STATE,
    voteRating: 5
  });
});
