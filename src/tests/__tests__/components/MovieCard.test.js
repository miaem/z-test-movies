import React from "react";
import { shallow } from "enzyme";
import { mockStore } from "./../../utils";
import { MovieCard } from "../../../components/card";

const initialState = {
  images: {
    isLoaded: true,
    breakpoints:
      "(min-width: 576px) 25vw, (min-width: 768px) 16.666667vw, 100vw",
    bySize: {},
    allSizes: []
  }
};

let store;

const init = (props = { id: 0, genres: [], title: "Title", image: {} }) => {
  return shallow(<MovieCard {...props} store={store} />);
};

beforeAll(() => {
  store = mockStore(initialState);
});

afterEach(() => {
  store.clearActions();
});

test("it renders", () => {
  let wrapper = init();
  expect(wrapper).toMatchSnapshot();
});

test("with props", () => {
  let props = {
    id: 920,
    genres: {
      "12": {
        id: 12,
        title: "Adventure"
      }
    },
    title: "Cars",
    image: {
      set:
        "https://image.tmdb.org/t/p/w92/jpfkzbIXgKZqCZAkEkFH2VYF63s.jpg 92w, https://image.tmdb.org/t/p/w154/jpfkzbIXgKZqCZAkEkFH2VYF63s.jpg 154w, https://image.tmdb.org/t/p/w185/jpfkzbIXgKZqCZAkEkFH2VYF63s.jpg 185w, https://image.tmdb.org/t/p/w342/jpfkzbIXgKZqCZAkEkFH2VYF63s.jpg 342w, https://image.tmdb.org/t/p/w500/jpfkzbIXgKZqCZAkEkFH2VYF63s.jpg 500w, https://image.tmdb.org/t/p/w780/jpfkzbIXgKZqCZAkEkFH2VYF63s.jpg 780w",
      src: "https://image.tmdb.org/t/p/original/jpfkzbIXgKZqCZAkEkFH2VYF63s.jpg"
    }
  };
  let wrapper = init(props);
  expect(wrapper).toMatchSnapshot();
});
