import React from "react";
import { shallow } from "enzyme";

import { MovieHeader } from "../../../components/header";

const init = (props = { title: "Title" }) =>
  shallow(<MovieHeader {...props} />);

test("it renders", () => {
  expect(init()).toMatchSnapshot();
});

test("with props", () => {
  let props = { title: "movieheader title" };
  let wrapper = init(props);
  expect(wrapper).toMatchSnapshot();
});
