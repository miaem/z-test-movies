import React from "react";
import { shallow } from "enzyme";
import { mockStore } from "./../utils";
import App from "../../App";

const initialState = {
  app: {
    isLoaded: false
  },
  images: {
    isLoaded: false,
    breakpoints: "..",
    bySize: {},
    allSizes: []
  },
  genres: {
    isLoaded: false,
    byId: {},
    allIds: []
  },
  movies: {
    isLoaded: false,
    byId: {},
    allIds: []
  }
};

let store;

const init = (props = {}) => shallow(<App {...props} store={store} />);

beforeAll(() => {
  store = mockStore(initialState);
});

afterEach(() => {
  store.clearActions();
});

test("it renders", () => {
  expect(init()).toMatchSnapshot();
});
