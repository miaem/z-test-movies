import _ from "lodash";
import React, { useEffect, useCallback, memo } from "react";
import { connect } from "react-redux";
import { getAppData } from "./store/app/actions";
import { updateMovieRating } from "./store/movies/actions";
import { MovieHeader } from "./components/header";
import { MovieCollection } from "./components/collections";
import { MovieRatingFilter } from "./components/filter";
import { ColfullWidth, ColfullWidthDivider, Row, Label } from "./AppStyles";

const FilterTitle = memo(({ title }) => {
  return (
    <ColfullWidth>
      <h2>{title}</h2>
    </ColfullWidth>
  );
});

const FilterLabel = memo(({ title }) => {
  return <Label htmlFor="rating-slide">{title}</Label>;
});

const Filter = memo(props => {
  return (
    <ColfullWidthDivider>
      <FilterLabel title="Popularity filter" />
      <MovieRatingFilter {...props} />
    </ColfullWidthDivider>
  );
});

const App = ({ getAppData, updateMovieRating }) => {
  useEffect(() => {
    getAppData();
  }, [getAppData]);

  const updateRating = useCallback(
    _.debounce(value => {
      updateMovieRating(value);
    }, 500),
    []
  );

  return (
    <React.Fragment>
      <MovieHeader title="Popular movies" data-test="component-header" />
      <main>
        <Row>
          <FilterTitle title="Filters:" data-test="component-filter-title" />
          <Filter onUpdate={updateRating} data-test="component-filter" />
        </Row>
        <MovieCollection data-test="component-movie-collection" />
      </main>
    </React.Fragment>
  );
};

export default connect(
  null,
  {
    getAppData,
    updateMovieRating
  }
)(memo(App));
