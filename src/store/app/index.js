import { GET_APP_DATA } from "./types";

const INITIAL_STATE = {
  isLoaded: false
};

/**
 * ! Reducers
 **/

export const appReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_APP_DATA:
      return {
        ...state,
        isLoaded: action.loaded
      };
    default:
      return { ...state };
  }
};
