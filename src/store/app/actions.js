import { GET_APP_DATA } from "./types";
import { fetchImages } from "./../images/actions";
import { fetchGenres } from "./../genres/actions";
import { fetchMovies } from "./../movies/actions";
import { getImages } from "./../images/actions";
import { getGenres } from "./../genres/actions";
import { getMovies } from "./../movies/actions";

export const getAppData = () => async dispatch => {
  const all = await Promise.all([
    dispatch(fetchImages()),
    dispatch(fetchGenres()),
    dispatch(fetchMovies())
  ]);
  const [images, genres, movies] = all;
  dispatch({
    type: GET_APP_DATA,
    loaded: true
  });
  dispatch(getImages(images));
  dispatch(getGenres(genres));
  dispatch(getMovies(movies));
};
