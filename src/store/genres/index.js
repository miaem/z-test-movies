import _ from "lodash";
import { FETCH_GENRES, GET_GENRES, ADD_MOVIE } from "./types";

const INITIAL_STATE = {
  isLoaded: false,
  byId: {},
  allIds: []
};

/**
 * ! Reducers
 **/

export const genresReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCH_GENRES:
      return {
        ...state,
        isLoaded: action.loaded
      };
    case GET_GENRES:
      return {
        ...state,
        byId: {
          ...state.byId,
          ..._.mapValues(_.keyBy(action.payload, "id"), item => {
            return {
              ...item,
              movies: []
            };
          })
        },
        allIds: [...state.allIds, ...action.payload.map(item => item.id)]
      };
    case ADD_MOVIE:
      return {
        ...state,
        byId: {
          ...state.byId,
          [action.genreId]: {
            ...state.byId[action.genreId],
            movies: [
              ...state.byId[action.genreId].movies,
              { id: action.movie.id, title: action.movie.title }
            ]
          }
        }
      };
    default:
      return { ...state };
  }
};
