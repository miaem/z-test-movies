import movies from "../../api/movies";
import { FETCH_GENRES, GET_GENRES, ADD_MOVIE } from "./types";
import { apiParams } from "../../api/config";

export const fetchGenres = () => async dispatch => {
  const response = await movies.get("/genre/movie/list", {
    params: { ...apiParams }
  });
  dispatch({ type: FETCH_GENRES, loaded: true });
  return response.data;
};

export const getGenres = response => {
  return {
    type: GET_GENRES,
    payload: response.genres,
    raw: response.genres.map(item => item.id)
  };
};

export const addMovieToGenre = (id, movie) => {
  return {
    type: ADD_MOVIE,
    genreId: id,
    movie: movie
  };
};
