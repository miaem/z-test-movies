import { createStore, combineReducers, applyMiddleware } from "redux";
import thunkMiddleware from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";

import { appReducer } from "./app";
import { imagesReducer } from "./images";
import { moviesReducer } from "./movies";
import { genresReducer } from "./genres";

export const rootReducer = combineReducers({
  app: appReducer,
  images: imagesReducer,
  genres: genresReducer,
  movies: moviesReducer
});

export default function stores() {
  const middlewares = [thunkMiddleware];
  const middleWareEnhancer = applyMiddleware(...middlewares);

  const store = createStore(
    rootReducer,
    composeWithDevTools(middleWareEnhancer)
  );

  return store;
}
