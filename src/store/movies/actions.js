import _ from "lodash";
import movies from "../../api/movies";
import { FETCH_MOVIES, GET_MOVIES, MOVIE_RATING } from "./types";
import { addMovieToGenre } from "../genres/actions";
import { apiParams, apiPopular } from "../../api/config";

export const fetchMovies = () => async dispatch => {
  const response = await movies.get(`${apiPopular}`, {
    params: { ...apiParams, page: 1 }
  });
  dispatch({
    type: FETCH_MOVIES,
    loaded: true
  });
  return response.data;
};

export const getMovies = movies => {
  return (dispatch, getState) => {
    dispatch({
      type: GET_MOVIES,
      payload:
        movies.results.length > 0
          ? _.mapValues(
              _.keyBy(
                movies.results.filter(
                  item => item.vote_average >= getState().movies.voteRating
                ),
                "id"
              ),
              item => {
                return {
                  ...item,
                  genre_ids: _.zipObject(
                    _.map(item.genre_ids, key => key),
                    _.map(item.genre_ids, val => {
                      dispatch(
                        addMovieToGenre(val, { id: item.id, title: item.title })
                      );
                      return {
                        id: val,
                        title: getState().genres.byId[val].name
                      };
                    })
                  ),
                  srcset: {
                    set: _.join(
                      _.map(
                        _.without(getState().images.allSizes, "original"),
                        val =>
                          `${getState().images.bySize[val].url}${
                            item.poster_path
                          } ${getState().images.bySize[val].size}w`
                      ),
                      ", "
                    ),
                    src: `${getState().images.bySize["original"].url}${
                      item.poster_path
                    }`
                  }
                };
              }
            )
          : {},
      raw: _.map(movies.results, "id")
    });
  };
};

export const updateMovieRating = val => {
  return {
    type: MOVIE_RATING,
    payload: val
  };
};
