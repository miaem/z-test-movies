import { FETCH_MOVIES, GET_MOVIES, MOVIE_RATING } from "./types";
import { createSelector } from "reselect";

const movieRating = state => state.movies.voteRating;
const moviesById = state => state.movies.byId;

const INITIAL_STATE = {
  isLoaded: false,
  voteRating: 3,
  byId: {},
  allIds: []
};

/**
 * ! Reducers
 **/

export const moviesReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCH_MOVIES:
      return {
        ...state,
        isLoaded: action.loaded
      };
    case GET_MOVIES:
      return {
        ...state,
        byId: {
          ...state.byId,
          ...action.payload
        },
        allIds: [...state.allIds, ...action.raw]
      };
    case MOVIE_RATING:
      return {
        ...state,
        voteRating: action.payload
      };
    default:
      return { ...state };
  }
};

/**
 * ! Selectors
 **/

export const getMoviesByRating = () =>
  createSelector(
    [movieRating, moviesById],
    (rating, items) => {
      return Object.keys(items)
        .filter(item => items[item].vote_average >= rating)
        .sort((a, b) => items[b].popularity - items[a].popularity)
        .map(val => items[val]);
    }
  );

export const getMovieRating = createSelector(
  movieRating,
  rating => rating
);
