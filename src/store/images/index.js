import _ from "lodash";
import { FETCH_IMAGES, GET_IMAGES } from "./types";
import { createSelector } from "reselect";

const imageBreakpoints = state => state.images.breakpoints;

const INITIAL_STATE = {
  isLoaded: false,
  breakpoints: "(min-width: 576px) 25vw, (min-width: 768px) 16.666667vw, 100vw",
  bySize: {},
  allSizes: []
};

/**
 * ! Reducers
 **/

export const imagesReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCH_IMAGES:
      return {
        ...state,
        isLoaded: action.loaded
      };
    case GET_IMAGES:
      return {
        ...state,
        bySize: {
          ...state.bySize,
          ..._.zipObject(
            _.map(action.payload.poster_sizes, key => key),
            _.map(action.payload.poster_sizes, val => {
              let sanitise = val.match(/\d+/g);
              return {
                url: `${action.payload.secure_base_url}${val}`,
                size: sanitise !== null ? sanitise.toString() : val
              };
            })
          )
        },
        allSizes: [...state.allSizes, ...action.payload.poster_sizes]
      };
    default:
      return { ...state };
  }
};

/**
 * ! Selectors
 **/

export const getImageBreakpoints = createSelector(
  imageBreakpoints,
  breakpoints => breakpoints
);
