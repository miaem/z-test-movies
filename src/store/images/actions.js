import movies from "../../api/movies";
import { FETCH_IMAGES, GET_IMAGES } from "./types";
import { apiParams } from "../../api/config";

export const fetchImages = () => async dispatch => {
  const response = await movies.get("/configuration", {
    params: { ...apiParams }
  });

  dispatch({
    type: FETCH_IMAGES,
    loaded: true
  });
  return response.data;
};

export const getImages = response => {
  const images = response.images;
  return {
    type: GET_IMAGES,
    payload: images,
    raw: images
  };
};
