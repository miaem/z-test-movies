## Popular movies

This project displays most popular movies using [TMDb](https://www.themoviedb.org) API

## Install

In the project directory, you can run to install dependencies:

### `npm i`

## Instructions

- Sign up and create a [TMDb API key](https://www.themoviedb.org/account/signup). 
- Create a .env file in the project directory
- Create a variable named `REACT_APP_API_KEY` in the .env file
- Assign TMDb API key to `REACT_APP_API_KEY`

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Build

I decided to use create-react-app in an attempt to get up to speed on all things React. For the styling I used [styled components](https://www.styled-components.com/) as brief mentioned not using css framework. In summary I tried to stick to brief while trying a few new things :sweat_smile: 

## Tests

I ran out of time and no tests have been created yet - hopefully everything runs smooth! :pray: it worked on chrome mac when I last checked :wink:
